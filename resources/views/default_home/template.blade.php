<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>
        label{
            font-size:14pt;
        }
    </style>
    <title>{{ env('APP_NAME') }}</title>
</head>
<body>
    <header>
        <h1 style="text-align:center;">Sistema de Login</h1>
    </header>
    <nav></nav>
    <div>
        @yield('content')
    </div>
    <footer></footer>
</body>
</html>