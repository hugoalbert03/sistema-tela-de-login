@extends('default_home/template')
@section('content')
    <form action="/valid" method="post">
        @csrf
        <div class="container">
            <h2>Acesso</h2>         
            <p>
                <label for="login">Login de Acesso</label>
                <input type="text" name="user" id="login" class="form-control" required>
            </p><p>
                <label for="senha">Senha</label>
                <input type="password" name="senha" id="senha" class="form-control" required>
            </p>
            @if(session("noAcessMsg"))
                <div class="alert alert-danger auto-fechar text-center">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-octagon" viewBox="0 0 16 16">
                        <path d="M4.54.146A.5.5 0 0 1 4.893 0h6.214a.5.5 0 0 1 .353.146l4.394 4.394a.5.5 0 0 1 .146.353v6.214a.5.5 0 0 1-.146.353l-4.394 4.394a.5.5 0 0 1-.353.146H4.893a.5.5 0 0 1-.353-.146L.146 11.46A.5.5 0 0 1 0 11.107V4.893a.5.5 0 0 1 .146-.353L4.54.146zM5.1 1 1 5.1v5.8L5.1 15h5.8l4.1-4.1V5.1L10.9 1H5.1z"/>
                        <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                    </svg>
                    <strong>{{session("noAcessMsg")}}</strong>
                </div>
            @endif
            <p>
                <a href=""class="" style="">Esqueceu a senha?</a>
            </p>  
            <button type="submit" class="btn btn-primary">Login</button>  
            <a href="/cadastro" class="btn btn-warning">Criar Cadastro</a>
        </div>
    </form>
@endsection