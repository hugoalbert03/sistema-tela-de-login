@extends('default_home/template')
@section('content')
    <form action="/addUser" method="post">
        @csrf
        <div class="container">
            <div class="row">
                <div class="col-sm-1">
                    <a href="/" class="btn btn-outline-primary">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-left-circle-fill" viewBox="0 0 16 16">
                        <path d="M8 0a8 8 0 1 0 0 16A8 8 0 0 0 8 0zm3.5 7.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5z"/>
                        </svg>
                        Voltar
                    </a>    
                </div>
                <div class="col-sm-3">
                    <h2>Criar Acesso</h2>
                </div>
            </div>
            <p>
                <label for="n_user">Nome de Usuário</label>
                <input type="text" name="nome" class="form-control" id="n_user" placeholder="Ex. Pedro" required>
            </p><p>
                <label for="acess">Acesso</label>
                <input type="text" name="acesso" class="form-control" id="acess" required>
            </p><p>
                <label for="senha">Digite sua senha</label>
                <input type="password" name="senha" class="form-control" id="senha" required>
            </p><p>
                <label for="senha-retry">Digite sua senha novamete!</label>
                <input type="password" class="form-control" id="senha-retry" required>
            </p>
            @if(session("msg"))
                <div class="alert alert-success auto-fechar text-center">
                    <strong>{{session('msg')}}</strong>
                </div>
            @endif
            <button type="submit" class="btn btn-success">Cadastrar</button>        
        </div>
    </form>
@endsection