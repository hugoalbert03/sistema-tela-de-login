<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\telaLoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('display/display-to-home');
});

Route::get('/login', [telaLoginController::class, 'loginView'])->name('login');

Route::get("/cadastro", [telaLoginController::class, 'cadastrar'])->name('cadastrar');

Route::post("/addUser", [telaLoginController::class, 'addNewUser'])->name('newUser');

Route::get("/loginAcepted", [telaLoginController::class, 'success'])->name('acepted');

Route::get("/loginDenied", [telaLoginController::class, 'userDenied'])->name('userDenied');

Route::post("/valid", [telaLoginController::class, 'valid'])->name('valid');