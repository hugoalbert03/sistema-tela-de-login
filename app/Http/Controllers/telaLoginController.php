<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Login;

class telaLoginController extends Controller
{
    //
    public function cadastrar(){
        return view('cadastro-usuario.cadastro');
    }

    public function loginView(){
        return view('display.display-to-home');
    }

    public function addNewUser(Request $request){
        #return view('display.display-to-home');
        Login::create($request->all());
        return redirect(route('cadastrar'))->with("msg","Usuário cadastrado com sucesso!");
    } 

    

    public function userDenied(){
        return redirect(route('login'))->with("noAcessMsg", "Login Incorreto!");
    }

    public function notExist(){
        return redirect(route('login'))->with("noAcessMsg", "Usuário Inexistente");
    }

    public function success(){
        #return view('success.user-acept');
        #return redirect(route('acepted'))->with("msg_User", $dados->nome)
    }

    public function valid(Request $request){
        $login_form = $request->all('user');
        $pw_login = $request->all('senha');
        #$login_db = Login::all('acesso')->find($login_form);
        $credencial = Login::where('id', $request->user)->first();
        $pw_db = Login::all();
        #$pw_db = Login::where('senha', $request->all('senha'));
        #$redirect = ($request->user == $credencial->acesso and $request->senha == $credencial->senha)?'/loginAcepted':'/loginDenied';
        #$redirect = ;
        #dd($redirect);
        $valid_user = isset($credencial->acesso)? $credencial->acesso:null;
        $valid_senha = isset($credencial->senha)? $credencial->acesso:null;
        if($request->user == $valid_user and $request->senha == $valid_senha){
            #return route('acepted');
            #return $this->success();
            return redirect(route('acepted'))->with("msg_User", $credencial->nome);
        } else {
            #return route('userDenied');
            return $this->notExist();
        }
    } 
}
