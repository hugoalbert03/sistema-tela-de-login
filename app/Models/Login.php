<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Login extends Model
{
    use HasFactory;
    protected $table = "usuario";
    protected $primaryKey = "id";

    const CREATED_AT = "date_create";
    const UPDATED_AT = "date_update";

    protected $fillable = [
        'nome', 
        'acesso', 
        'senha'
    ];
}
